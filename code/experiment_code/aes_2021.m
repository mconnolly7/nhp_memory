paper_dir = '/Users/mconn24/Dropbox/research/Conferences/AES/AES_2021/memory_abstract/';
data_table_path = [paper_dir 'dory_database_AES_2021.xlsx'];

data_table = readtable(data_table_path);

ds              = [];
encode_A        = [];
pcn_injected    = [];
both_objects    = [];
either_object   = [];

for c1 = 1:size(data_table,1)
    
    aoi_path = [paper_dir data_table.aoi_path{c1} '.xlsx'];
    
    if exist(aoi_path, 'file')
        [ds_, encode_A_, both_objects_, either_object_] = aoi_to_ds(aoi_path);
    
        ds              = [ds; ds_];
        encode_A        = [encode_A; encode_A_];
        pcn_injected    = [pcn_injected; ones(size(ds_)) * data_table.daysSinceLastPCNDose(c1) == 0];
        both_objects    = [both_objects; both_objects_];
        either_object   = [either_object; either_object_];

    end
end

%%
anova1(ds(both_objects == 1), pcn_injected(both_objects == 1))
ttest2(both_objects(pcn_injected == 1), both_objects(pcn_injected == 0))
%%

% x(1) = sum(both_objects & pcn_injected);
% x(2) = sum(both_objects & ~pcn_injected);
% x(3) = sum(~both_objects & ~pcn_injected);
% x(4) = sum(~both_objects & pcn_injected);
% [h p c] = fishertest(x)
for c1 = 3%1:35
    is_valid = encode_A > c1;
    is_valid = both_objects;

    x(1,1) = sum(pcn_injected & is_valid);
    x(1,2) = sum(pcn_injected & ~is_valid);
    x(2,1) = sum(~pcn_injected & is_valid);
    x(2,2) = sum(~pcn_injected & ~is_valid);
    [h p(c1) c] = fishertest(x);
end
% [h p c] = ttest2(encode_A(pcn_injected == 0),encode_A(pcn_injected == 1))
p(6)

x
