paper_dir               = '/Users/mconn24/OneDrive/papers/2020_08_01_paper_NHP_PCN_memory/';

data_table_path         = [paper_dir 'pcn_memory_meta_database.xlsx'];
data_table              = readtable(data_table_path);

aoi_table               = organize_AOI_data(paper_dir, data_table);

%%
save_path               = [paper_dir 'sac_database_2.csv'];
sac_table               = organize_SAC_data(paper_dir, data_table, save_path);

%%
merged_table            = outerjoin(data_table, aoi_table, 'Keys', {'experiment_uid'}, 'MergeKeys', 1);
merged_table            = outerjoin(merged_table, sac_table, 'Keys', {'experiment_uid', 'trial_idx'}, 'MergeKeys', 1);

%%
ds                      = (merged_table.retrieve_B - merged_table.retrieve_A) ./ (merged_table.retrieve_B + merged_table.retrieve_A);
visit_duration          = merged_table.retrieve_A;
% visit_duration          = merged_table.retrieve_B;
visit_duration          = merged_table.retrieve_A + merged_table.retrieve_B;
cummulative_pcn         = merged_table.cummulative_pcn;
%%
subject_idx             = strcmp(merged_table.subject_id, 'Dory');
[a b] = ttest(ds(subject_idx))

%%
clc
close all 
subjects                = {'Waldo', 'KP', 'Casper'};

predictor               = merged_table.days_since_pcn; 
for c1 = 1:size(subjects,2)
    fprintf(subjects{c1})
    subject_idx             = strcmp(merged_table.subject_id, subjects{c1});

    days_since_pcn                  = merged_table.days_since_pcn;
    baseline_idx                    = isnan(days_since_pcn);
%     days_since_pcn(baseline_idx)    = -1;

    valid_idx                       = ~isnan(visit_duration) & ~isnan(days_since_pcn) ...
        & days_since_pcn < 12 & days_since_pcn > 0  & subject_idx;

%     G(baseline_idx)         = -1;      
%     G(d1_idx)               = 1;      
%     G(dn_idx)               = 1;    
    % anova1(visit_duration,G)
%     [a b] = corr(days_since_pcn(valid_idx & subject_idx), visit_duration(valid_idx & subject_idx))
%     hold on
%     scatter(days_since_pcn(valid_idx & subject_idx), visit_duration(valid_idx & subject_idx))
    
    X = [days_since_pcn(valid_idx) cummulative_pcn(valid_idx)];
    y = visit_duration(valid_idx);
    
    models{c1} = fitlm(X, y, 'visit_duration ~ 1 +  days_since_pcn + cummulative_pcn', 'VarNames', {'days_since_pcn', 'cummulative_pcn', 'visit_duration'});
end