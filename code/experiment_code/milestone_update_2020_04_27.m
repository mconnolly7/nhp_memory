aoi_table = organize_AOI_data;

%%
close all
ds  = (aoi_table.retrieve_B - aoi_table.retrieve_A) ./ (aoi_table.retrieve_B + aoi_table.retrieve_A);
G   = aoi_table.stimulation_pattern;

valid_idx   = aoi_table.encode_A > 5 & aoi_table.encode_A < 35 ;

ds_valid    = ds(valid_idx);
G_valid     = G(valid_idx);
p           = anova1(ds_valid, G_valid, 'off');

hold on
boxplot(ds_valid, G_valid);
xticklabels({'Sham', 'Ring Bipolar', 'Wide Bipolar', 'Dual Ring Bipolar', 'Full ADMES'})
ylabel('DS')
set(gca,'FontSize', 16)

a       = get(get(gca,'children'),'children');   % Get the handles of all the objects
t       = get(a,'tag');   % List the names of all the objects 
med_ax  = strcmp(t, 'Median');

set(a, 'LineWidth', 2);   % Set the color of the first box to green
set(a(~med_ax), 'Color', 'k');   % Set the color of the first box to green

plot([1 3 4 5], [1 1 1 1]+.1, 'k*', 'MarkerSize', 10)
box off
% title(sprintf('P-value: %.4f', p))