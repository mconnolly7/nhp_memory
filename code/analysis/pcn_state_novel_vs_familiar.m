function pcn_state_novel_vs_familiar(pcn_state, subject_id, retrieve_A, retrieve_B)
unique_subjects = unique(subject_id);


dark2 = [27,158,119
217,95,2
117,112,179]/255;
for c1 = 1:size(unique_subjects,1)
    
    subject_idx             = subject_id == unique_subjects(c1);
    A_subject               = retrieve_A(subject_idx);
    B_subject               = retrieve_B(subject_idx);
    pcn_state_subject     	= pcn_state(subject_idx);
    valid_idx               = ~(isnan(A_subject) | isnan(B_subject));
    
    baseline_A              = A_subject(valid_idx & pcn_state_subject==0);
    baseline_B              = B_subject(valid_idx & pcn_state_subject==0);
    pcn_A                   = A_subject(valid_idx & pcn_state_subject==1);
    pcn_B                   = B_subject(valid_idx & pcn_state_subject==1);
    
    hold on
    x1                      = -.25*ones(size(baseline_A))+c1 + (rand(size(baseline_A))-.5)/15;
    x2                      = -.1*ones(size(baseline_B))+c1 + (rand(size(baseline_B))-.5)/15;
    x3                      = .1*ones(size(pcn_A))+c1 + (rand(size(pcn_A))-.5)/15;
    x4                      = .25*ones(size(pcn_B))+c1 + (rand(size(pcn_B))-.5)/15;
    
    m1                      = mean(baseline_A);
    m2                      = mean(baseline_B);
    m3                      = mean(pcn_A);
    m4                      = mean(pcn_B);
    
    s1                      = std(baseline_A) / sqrt(size(baseline_A,1));
    s2                      = std(baseline_B) / sqrt(size(baseline_B,1));
    s3                      = std(pcn_A) / sqrt(size(pcn_A,1));
    s4                      = std(pcn_B) / sqrt(size(pcn_B,1));
    
    [h(c1,1), p(c1,1)]      = ranksum(baseline_A, baseline_B);
    [h(c1,2), p(c1,2)]      = ranksum(pcn_A, pcn_B);
    
%     scatter(x1, baseline_A, [], .5*ones(1,3))
    h1 = bar(-.25 + c1, m1,  1/10, 'FaceColor', .8*ones(1,3), 'LineWidth', 2);
    plot((-.25 + c1) * [1 1], [m1-s1  m1+s1], 'LineWidth', 2, 'Color', 'k');
    
%     scatter(x2, baseline_B, [],  [0.5843    0.8157    0.9882])
    h2(c1) = bar(-.1 + c1, m2,  1/10, 'FaceColor', dark2(c1,:), 'FaceAlpha', .2,'LineWidth', 2, 'EdgeColor', dark2(c1,:));
    plot((-.1 + c1) * [1 1], [m2-s2  m2+s2], 'LineWidth', 2, 'Color', dark2(c1,:));

%     scatter(x3, pcn_A, [], .5*ones(1,3))
    bar(.1 + c1, mean(pcn_A),  1/10, 'FaceColor', .8*ones(1,3), 'LineWidth', 2);
    plot((.1 + c1) * [1 1], [m3-s3  m3+s3], 'LineWidth', 2, 'Color', 'k')

%     scatter(x4, pcn_B, [], [0.5843    0.8157    0.9882])
    bar(.25 + c1, mean(pcn_B),  1/10, 'FaceColor', dark2(c1,:), 'FaceAlpha', .2, 'LineWidth', 2, 'EdgeColor', dark2(c1,:));
    plot((.25 + c1) * [1 1], [m4-s4  m4+s4], 'LineWidth', 2, 'Color', dark2(c1,:))
    
end

xlim([.5 3.5])
ylim([0 3]);
box off
xticks([.825 1.175 1.825 2.175 2.825 3.175])
xticklabels({'Baseline', 'PCN','Baseline', 'PCN','Baseline', 'PCN'})
ylabel('Visit Duration (s)')
set(gca,'FontSize', 12)
set(gcf,'color','w')

annotation('textbox', [0.17, 0.07, 0, 0], 'string', 'NHP1', 'FontSize', 15)
annotation('textbox', [0.282, 0.07, 0, 0], 'string', 'NHP2', 'FontSize', 15)
annotation('textbox', [0.392, 0.07, 0, 0], 'string', 'NHP3', 'FontSize', 15)
legend([h1 h2], {'Familiar Image', '', 'Novel Image', ''}, 'Location', 'northeast')

p
h
end