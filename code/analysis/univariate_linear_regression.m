function univariate_linear_regression(X, Y, subject_id)

dark2 = [27,158,119
217,95,2
117,112,179]/255;
unique_subjects = unique(subject_id);

for c1 = 1:size(unique_subjects,1)
    
    subject_idx     = subject_id == unique_subjects(c1);
    X_subject       = X(subject_idx);
    Y_subject       = Y(subject_idx);
    
    val_idx         = ~(isnan(X_subject) | isnan(Y_subject));
    p               = polyfit(X_subject(val_idx), Y_subject(val_idx), 1);
    y               = polyval(p, X_subject(val_idx));
    [rho, p_val]   	= corr(X_subject(val_idx), Y_subject(val_idx), 'Type', 'spearman');
    
    hold on
    h(c1) = scatter(X_subject(val_idx), Y_subject(val_idx), 50, dark2(unique_subjects(c1),:) ,'filled', 'MarkerEdgeColor', 'k', 'MarkerFaceAlpha', .5);
    h_line(c1)= plot(X_subject(val_idx), y, 'color', dark2(unique_subjects(c1),:), 'LineWidth', 3);
    
    legend_str{c1} = sprintf('NHP%d, rho=%.2f, p-value=%.2f', unique_subjects(c1), rho, p_val);
end

legend(h,legend_str)
uistack(h_line,'top');
end
