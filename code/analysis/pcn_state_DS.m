function pcn_state_DS(pcn_state, subject_id, discriminant_score, days_since_pcn)

baseline_idx            = pcn_state == 0;
interictal_idx          = pcn_state == 1 & days_since_pcn > 0;
seizure_idx             = pcn_state == 1 & days_since_pcn == 0;
ds_valid_idx            = ~isnan(discriminant_score);
    
unique_subjects         = unique(subject_id);
unique_subjects(cellfun(@isempty, unique_subjects)) = [];

for c1 = 1:size(unique_subjects,1)
    subject_idx             = strcmp(subject_id, unique_subjects(c1));
 
    ds_baseline             = discriminant_score(subject_idx & ds_valid_idx & baseline_idx);
    ds_interictal           = discriminant_score(subject_idx & ds_valid_idx & interictal_idx);
    ds_seizure              = discriminant_score(subject_idx & ds_valid_idx & seizure_idx);
   
    Y                       = [ds_baseline; ds_interictal; ds_seizure]; 
    G                       = [1*ones(size(ds_baseline)); 2*ones(size(ds_interictal)); 3*ones(size(ds_seizure))];
    figure
    boxplot(Y,G, 'notch', 'on')
    hold on
    plot([0 4], [0 0])
    xticklabels({'Baseline', 'Interictal','Seizure', 'PCN','Baseline', 'PCN'})
    title(unique_subjects(c1));
    ylabel('Discrimination Score')
    
%     x1                      = -.25*ones(size(baseline_A))+c1 + (rand(size(baseline_A))-.5)/15;
%     x2                      = -.1*ones(size(baseline_B))+c1 + (rand(size(baseline_B))-.5)/15;
%     x3                      = .1*ones(size(pcn_A))+c1 + (rand(size(pcn_A))-.5)/15;
%     
%     m1                      = mean(ds_baseline);
%     m2                      = mean(ds_interictal);
%     m3                      = mean(ds_seizure);
%     
%     s1                      = std(ds_baseline) / sqrt(size(ds_baseline,1));
%     s2                      = std(ds_interictal) / sqrt(size(ds_interictal,1));
%     s3                      = std(ds_seizure) / sqrt(size(ds_seizure,1));
%     
%     [h(c1,1), p(c1,1)]      = ranksum(baseline_A, baseline_B);
%     [h(c1,2), p(c1,2)]      = ranksum(pcn_A, pcn_B);
%     
%     scatter(x1, baseline_A, [], .5*ones(1,3))
%     bar(-.25 + c1, m1,  1/10, 'FaceColor', 'none', 'LineWidth', 2);
%     h1 = plot((-.25 + c1) * [1 1], [m1-s1  m1+s1], 'LineWidth', 2, 'Color', 'k');
%     
%     scatter(x2, baseline_B, [],  [0.5843    0.8157    0.9882])
%     bar(-.1 + c1, m2,  1/10, 'FaceColor', 'none', 'LineWidth', 2, 'EdgeColor', [0 0 .5]);
%     h2 = plot((-.1 + c1) * [1 1], [m2-s2  m2+s2], 'LineWidth', 2, 'Color', [0 0 .5]);
% 
%     scatter(x3, pcn_A, [], .5*ones(1,3))
%     bar(.1 + c1, mean(pcn_A),  1/10, 'FaceColor', 'none', 'LineWidth', 2);
%     plot((.1 + c1) * [1 1], [m3-s3  m3+s3], 'LineWidth', 2, 'Color', 'k')
% 
%     scatter(x4, pcn_B, [], [0.5843    0.8157    0.9882])
%     bar(.25 + c1, mean(pcn_B),  1/10, 'FaceColor', 'none', 'LineWidth', 2, 'EdgeColor', [0 0 .5]);
%     plot((.25 + c1) * [1 1], [m4-s4  m4+s4], 'LineWidth', 2, 'Color', [0 0 .5])
% 
%     xlim([.5 3.5])
%     ylim([0 5]);
%     box off
% end
% 
% xticks([.825 1.175 1.825 2.175 2.825 3.175])
% xticklabels({'Baseline', 'PCN','Baseline', 'PCN','Baseline', 'PCN'})
% ylabel('Visit Duration (s)')
% set(gca,'FontSize', 12)
% set(gcf,'color','w')
% 
% annotation('textbox', [0.22, 0.07, 0, 0], 'string', 'NHP1', 'FontSize', 15)
% annotation('textbox', [0.485, 0.07, 0, 0], 'string', 'NHP2', 'FontSize', 15)
% annotation('textbox', [0.735, 0.07, 0, 0], 'string', 'NHP3', 'FontSize', 15)
% legend([h1 h2], {'Familiar Image', 'Novel Image'}, 'Location', 'northwest')
% 
% p
% h
end