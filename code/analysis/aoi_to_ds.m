function [ds, encode_A, both_objects, either_object] = aoi_to_ds(aoi_path)

% Load AOI data
raw_aoi_table       = readtable(aoi_path);
    
% Reshape to create matrix
%   column is trial
%   rows are image/phase 
aoi_mat             = reshape(raw_aoi_table{1,2:end}, 5, [])';
image_names        	= raw_aoi_table.Properties.VariableNames(2:5:end);

% Resort the matrix based on order of presentation
clear image_idx
for c2 = 1:size(image_names,2)
    [start_idx, end_idx]    = regexp(image_names{c2}, '\d+');
    image_idx(c2)           = str2double(image_names{c2}(start_idx:end_idx));     
end

aoi_table        = array2table(aoi_mat, 'VariableNames', ...
    {'encode_A','retrieve_A','retrieve_B','retrieve_A_flip','retrieve_C_flip'});

ds              = (aoi_table.retrieve_B - aoi_table.retrieve_A) ./ (aoi_table.retrieve_B + aoi_table.retrieve_A);
encode_A        = aoi_table.encode_A;
both_objects    = aoi_table.retrieve_A > 0 & aoi_table.retrieve_B > 0;
either_object   = aoi_table.retrieve_A > 0 | aoi_table.retrieve_B > 0;
end

