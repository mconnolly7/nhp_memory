function this_one(training_spike_rate, discrimination_score, pcn_state)
dark2 = [27,158,119
217,95,2
117,112,179]/255;


baseline_ds     = discrimination_score(pcn_state==0);
no_ii_ds        = discrimination_score(pcn_state==1 & training_spike_rate == 0);
with_ii_ds      = discrimination_score(pcn_state==1 & training_spike_rate > 0);



n1  = size(baseline_ds,1);
n2  = size(no_ii_ds,1);
n3  = size(with_ii_ds,1);

Y    = [baseline_ds; no_ii_ds; with_ii_ds];
G    = [1*ones(n1,1); 2*ones(n2,1);3*ones(n3,1)];

boxplot(Y,G, 'Notch', 'on');
hold on
a = get(get(gca,'children'),'children');   % Get the handles of all the objects
set(a, 'Color', dark2(3,:), 'linewidth', 3); 
set(a(4:6), 'Color', 'r', 'linewidth', 3); 

plot([1 2 3], [nanmean(baseline_ds) nanmean(no_ii_ds) nanmean(with_ii_ds)], 'x', 'MarkerSize', 20, 'LineWidth', 3, 'color', 'k')
xticklabels({'Baseline', 'PCN, no II','PCN, with II'})
ylabel('Discrimination Score') 
set(gcf,'color','w')
set(gca,'FontSize', 16)


end

