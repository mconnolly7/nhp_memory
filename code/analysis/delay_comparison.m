function delay_comparison

% Using this analysis to refactor the processing and merging of the
% different datasets
paper_dir               = '/Users/mconn24/OneDrive/papers/2020_08_01_paper_NHP_PCN_memory/';


merged_table            = process_data_from_directory(paper_dir);

retrieve_A
retrieve_B
delay

close all
dark2 = [27,158,119
217,95,2
117,112,179]/255;

delay_10 = delay < 3e4;
delay_60 = delay > 3e4;

A_10    = retrieve_A(delay_10);
B_10    = retrieve_B(delay_10);
A_60    = retrieve_A(delay_60);
B_60    = retrieve_B(delay_60);

ds      = (retrieve_B - retrieve_A) ./ (retrieve_B + retrieve_A);
ds_10   = ds(delay_10); 
ds_60   = ds(delay_60);

n10  = size(A_10,1);
n60  = size(A_60,1);
Y    = [A_10; B_10; A_60; B_60];
G    = [1*ones(n10,1); 2*ones(n10,1);3*ones(n60,1);4*ones(n60,1)];

subplot(1,2,1)

a = plotSpread({A_10  B_10  A_60  B_60}, 'distributionColors', dark2(3,:), 'categoryMarkers', 'o', 'spreadWidth', .7);

boxplot(Y,G, 'Notch', 'on', 'Symbol', '');
hold on
a = get(get(gca,'children'),'children');   % Get the handles of all the objects
set(a{1}, 'Color', 'k', 'linewidth', 5); 
set(a{1}(5:8), 'Color', 'r', 'linewidth', 5); 

plot([1 2 3 4 ], [mean(A_10) mean(B_10) mean(A_60) mean(B_60)], 'x', 'MarkerSize', 20, 'LineWidth', 3, 'color', 'k')
xticklabels({'Familiar 10s', 'Novel 10s','Familiar 60s','Novel 60s'})
ylabel('Visit Duration(s)') 
set(gcf,'color','w')
set(gca,'FontSize', 16)
ylim([0 5])
box off

subplot(1,2,2)
Y    = [ds_10; ds_60];
G    = [1*ones(n10,1); 2*ones(n60,1)];

a = plotSpread({ds_10 ds_60}, 'distributionColors', dark2(3,:), 'categoryMarkers', 'o', 'spreadWidth', .3);
hold on
boxplot(Y,G, 'Notch', 'on');
hold on
a = get(get(gca,'children'),'children');   % Get the handles of all the objects
set(a{1}, 'Color', dark2(3,:), 'linewidth', 5); 
set(a{1}, 'Color', 'k', 'linewidth', 5); 
set(a{1}(3:4), 'Color', 'r', 'linewidth', 5); 


% set(a{1}, 'MarkerSize', 15)         
% set(a{1}, 'MarkerFacecolor', [.5 .5 .5 .5])         

plot([1 2], [mean(ds_10) mean(ds_60)], 'x', 'MarkerSize', 20, 'LineWidth', 3, 'color', 'k')
xticklabels({'Delay 10s', 'Delay 60s'})
ylabel('DS') 
set(gcf,'color','w')
set(gca,'FontSize', 16)
box off



end

