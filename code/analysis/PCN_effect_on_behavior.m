close all
merged_table = merge_data;

%
% Effect of repeated PCN induced seizure on memory and behavior in the VPC task 
%

% Dependent
encode_A                = merged_table.encode_A;
retrieve_A              = merged_table.retrieve_A;
retrieve_B              = merged_table.retrieve_B;
discriminant_score      = (retrieve_B - retrieve_A)./(retrieve_B + retrieve_A);
% discriminant_score      = (retrieve_B - retrieve_A);

training_time           = merged_table.training_time;

train_fixation_time     = merged_table.training_fixation ./ training_time ;
test_fixation_time      = merged_table.testing_fixation;

train_saccade_time      = merged_table.training_saccade / 1000;
test_saccade_time       = merged_table.testing_saccade;


% Independent
cummulative_PCN         = merged_table.cummulative_pcn;
days_since_pcn          = merged_table.days_since_pcn;
last_pcn_dose           = merged_table.last_pcn_dose;
PCN_number              = merged_table.PCN_number;

for c1 = 1:size(merged_table.pcn_state,1)
    switch merged_table.pcn_state{c1}
        case 'baseline'
            pcn_state(c1,1) = 0;
        case 'Baseline'
            pcn_state(c1,1) = 0;
        case 'PCN'
            pcn_state(c1,1) = 1;
    end
end

for c1 = 1:size(merged_table.training_spike_codes,1)
    training_spikes             = merged_table.training_spike_codes{c1};
    training_spike_rate(c1,1)   = sum(training_spikes > 0) / training_time(c1) * 1000;
end

% Control
subject_id              = merged_table.subject_id;
subject_idx             = zeros(size(subject_id));
subject_idx(strcmp(subject_id,'Waldo'))     = 1;
subject_idx(strcmp(subject_id,'Casper'))    = 2;
subject_idx(strcmp(subject_id,'KP'))        = 3;

delay                   = merged_table.delay;

for c1 = 1:size(delay,1)
    delay_rounded   = [10 60];
    [~, min_idx]    = min(abs(delay(c1)/1000 - delay_rounded));
    delay(c1)       = delay_rounded(min_idx);
end

%%
figure( 'Position',  [190, 100, 1600, 500])
subplot(1,2,1)
control_idx         = delay == 10 ;
pcn_state_novel_vs_familiar(pcn_state(control_idx), subject_idx(control_idx), retrieve_A(control_idx), retrieve_B(control_idx)); 

%
subplot(1,2,2)
dependent_variable  = retrieve_A + retrieve_B;
univariate_linear_regression(cummulative_PCN(control_idx)/1000, dependent_variable(control_idx), subject_idx(control_idx))
xlabel('Cumulative PCN (kIU)')
ylabel('Visit Duration')
xlim([-10 210])
ylim([-.1 5.5])
set(gca, 'FontSize', 15)
set(gcf,'color','w')

%%
close all
figure( 'Position',  [190, 100, 1600, 500])
% control_idx         = encode_A > 5 & strcmp(subject_id, 'KP');
control_idx         = encode_A > 5 & strcmp(subject_id, 'KP') & days_since_pcn == 0;
delay_comparison(retrieve_A(control_idx), retrieve_B(control_idx), delay(control_idx))
xticklabels({'Familiar 10s', 'Novel 10s','Familiar 60s','Novel 60s'})
title('Days since PCN == 0')
ylabel('Visit Duration(s)') 
set(gcf,'color','w')
ylim([-.1 5.5])
box off

%%
% close all
control_idx         = delay == 10 & encode_A > 5;
control_idx         = delay == 10 & encode_A > 5 & days_since_pcn > 0;
control_idx         = delay == 10 & encode_A > 5 & days_since_pcn > 0 & training_spike_rate > 0 & subject_idx == 3;

dependent_variable  = retrieve_A + retrieve_B;
univariate_linear_regression(training_spike_rate(control_idx), dependent_variable(control_idx), subject_idx(control_idx))
xlabel('Training spike rate (s^{-1})')
ylabel('Visit Duration (s)')
set(gca,'FontSize', 15);
% title('delay == 10 & encode A > 5 ')
% title('delay == 10 & encode A > 5 & days since pcn > 0 ')
% title('delay == 10 & encode A > 5 & days since pcn > 0 & training spike rate > 0')
% ylabel('Visit Duration')
% xlim([-10 210])
% ylim([-.1 5.5])
% set(gca, 'FontSize', 15)
% set(gcf,'color','w')
%%
control_idx         = delay == 10  & retrieve_A > 0 & retrieve_B > 0 ;
control_idx         = delay == 10  & retrieve_A + retrieve_B > 0 & encode_A > 5;
pcn_state_DS(pcn_state(control_idx), subject_id(control_idx), discriminant_score(control_idx), days_since_pcn(control_idx)); 

%%
close all
control_idx         = delay == 10  & encode_A > 5 & subject_idx == 3;
this_one(training_spike_rate(control_idx), discriminant_score(control_idx), pcn_state(control_idx))
box off

























