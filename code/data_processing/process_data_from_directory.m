% function merged_table = process_data_from_directory(paper_dir)

paper_dir               = '/Users/mconn24/OneDrive - Emory University/papers/2020_08_01_paper_NHP_PCN_memory/data/';

data_table_path         = [paper_dir 'pcn_memory_meta_database.xlsx'];
data_table              = readtable(data_table_path);


aoi_table_path          = [paper_dir 'aoi_database.csv'];
aoi_table               = readtable(aoi_table_path);

sac_table_path          = [paper_dir 'sac_database_2.csv'];
sac_table               = readtable(sac_table_path);

sound_interictal_path  	= [paper_dir 'sound_interictal_database.mat'];
sound_interictal_table  = load(sound_interictal_path);
sound_interictal_table  = sound_interictal_table.sound_interictal_table;

aoi_table               = organize_AOI_data(paper_dir, data_table, save_path);
sac_table               = organize_SAC_data(paper_dir, data_table, save_path);


%%
% Combine metadata with AOI
merged_table            = outerjoin(data_table, aoi_table, 'Keys', {'experiment_uid'}, 'MergeKeys', 1);

% Add sac data
merged_table            = outerjoin(merged_table, sac_table, 'Keys', {'experiment_uid', 'trial_idx'}, 'MergeKeys', 1);

% Add sound-interictal data
merged_table            = outerjoin(merged_table, sound_interictal_table, 'Keys', {'experiment_uid', 'trial_idx'}, 'MergeKeys', 1);

% end
















