paper_dir           = '/Users/mconn24/Box Sync/papers/2020_08_01_paper_NHP_PCN_memory/';
data_table_path     = [paper_dir 'pcn_memory_meta_database.xlsx'];
save_path           = [paper_dir 'sac_database.csv'];
data_table          = readtable(data_table_path);
gaze_event_table    = [];

for c1 = 61:size(data_table,1)
    fprintf('%d ',  c1);
    sac_file_name       = data_table.SAC_data_path{c1};
    sac_path            = [paper_dir sac_file_name];
    sac_path            = strrep(sac_path, '.xlsx', '.mat');
    save_path           = strrep(sac_path, '.mat', '_2.mat');
    
    if ~exist(sac_path, 'file')  || isempty(sac_file_name)
        continue;
    end
    
    load(sac_path);

    % Load SAC data 
    
    % Parse out image indicies
    image_idx         	= zeros(size(raw_sac_table,1),1);
    image_phase        	= cell(size(raw_sac_table,1),1);
    image_phase(:)      = {''};
    tic 
    for c2 = 1:size(raw_sac_table,1)   
        image_name  = raw_sac_table.MediaName{c2};
        tokens      = regexp(image_name, 'VPC(\d+)([abc])','tokens');
        
        if ~isempty(tokens)
            image_idx(c2)   = str2double(tokens{1}{1});
            image_phase{c2} = tokens{1}{2};          
        end
    end
    
    fprintf('Parse time = %.2f\n',toc);
    raw_sac_table = [raw_sac_table table(image_idx, image_phase)];
    
    save(save_path, 'raw_sac_table');
end