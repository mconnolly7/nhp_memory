paper_dir           = '/Users/mconn24/Box Sync/papers/2020_08_01_paper_NHP_PCN_memory/';
data_table_path     = [paper_dir 'pcn_memory_meta_database.xlsx'];
save_path           = [paper_dir 'sac_database.csv'];
data_table          = readtable(data_table_path);
gaze_event_table    = [];

for c1 = 61:size(data_table,1)
    c1
    sac_file_name       = data_table.SAC_data_path{c1};
    sac_path            = [paper_dir sac_file_name];
    
    if isempty(sac_file_name)
        continue;
    end
    
    tic
    raw_sac_table       = readtable(sac_path);
    for c1 = 1:10; beep; pause(.1);end
    
    GazeEventType       = raw_sac_table.GazeEventType;
    GazeEventDuration   = raw_sac_table.GazeEventDuration;
    RecordingTimestamp  = raw_sac_table.RecordingTimestamp;
    MediaName           = raw_sac_table.MediaName;
    raw_sac_table       = table(GazeEventType, GazeEventDuration, RecordingTimestamp, MediaName);
    
    
    read_table_time(c1) = toc;
    
    save_path           = strrep(sac_path, '.xlsx', '.mat');
    save(save_path, 'raw_sac_table')
    
    tic
    load(save_path)
    load_mat_time(c1)   = toc;
end

