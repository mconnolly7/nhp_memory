function sac_table = organize_SAC_data(paper_dir, data_table, save_path)

% paper_dir           = '/Users/mconn24/Box Sync/papers/2020_08_01_paper_NHP_PCN_memory/';
% data_table_path     = [paper_dir 'pcn_memory_meta_database.xlsx'];
% save_path           = [paper_dir 'sac_database_2.csv'];
% data_table          = readtable(data_table_path);
sac_table    = [];

for c1 = 1:size(data_table,1)
   
    sac_file_name       = data_table.SAC_data_path{c1};
    sac_path            = [paper_dir sac_file_name];
    sac_path            = strrep(sac_path, '.xlsx', '_2.mat'); % Use converted file for faster load time
    
    if ~exist(sac_path, 'file') || isempty(sac_file_name)
        continue;
    end
    
    % Load SAC data 
    fprintf('%d, ',  c1);
    load(sac_path);

    % Parse out image indicies
    image_idx                   = raw_sac_table.image_idx;
    image_phase                	= raw_sac_table.image_phase;
    
    % Organize trials
    unique_image_idx            = unique(image_idx);
    unique_image_idx(1)         = [];
    
    fixation_idx                = strcmp(raw_sac_table.GazeEventType, 'Fixation');
    saccade_idx                 = strcmp(raw_sac_table.GazeEventType, 'Saccade');

    clear training_fixation training_saccade testing_fixation testing_saccade delay training_time
    for c2 = 1:size(unique_image_idx,1)
        trial_idx               = image_idx == unique_image_idx(c2);
        phase_a_idx             = strcmp(image_phase, 'a');

        training_fixation(c2)   = sum(fixation_idx & trial_idx & phase_a_idx) * 1000/120;
        training_saccade(c2)    = sum(saccade_idx & trial_idx & phase_a_idx) * 1000/120;
        
        phase_b_idx             = strcmp(image_phase, 'b');
        
        testing_fixation(c2)    = sum(fixation_idx & trial_idx & phase_b_idx) * 1000/120;
        testing_saccade(c2)     = sum(saccade_idx & trial_idx & phase_b_idx) * 1000/120;
        
        phase_a_start_idx       = find(phase_a_idx & trial_idx, 1);
        phase_a_end_idx         = find(phase_a_idx & trial_idx, 1,'last');
        phase_b_start_idx       = find(phase_b_idx & trial_idx, 1);
        
        phase_a_start_time      = raw_sac_table.RecordingTimestamp(phase_a_start_idx);
        phase_a_end_time        = raw_sac_table.RecordingTimestamp(phase_a_end_idx);
        phase_b_start_time      = raw_sac_table.RecordingTimestamp(phase_b_start_idx);
        
        
        if ~isempty(phase_a_start_time) && ~isempty(phase_a_end_time)
            training_time(c2) 	= phase_a_end_time - phase_a_start_time;
            if training_time(c2) < 0
                training_time(c2)
            end
        else
            training_time(c2)   = nan;
        end
        
        if ~isempty(phase_b_start_time) && ~isempty(phase_a_end_time)
            delay(c2)         	= phase_b_start_time - phase_a_end_time;
        else
            delay(c2)           = nan;
        end
    end
    
    experiment_uid              = repmat(data_table.experiment_uid(c1), size(unique_image_idx));
    
    gaze_event_sub_table        = table(unique_image_idx, training_fixation', training_saccade', ...
        testing_fixation', testing_saccade', training_time', delay', experiment_uid, ...
        'VariableNames', {'trial_idx','training_fixation', 'training_saccade', 'testing_fixation', ...
        'testing_saccade','training_time', 'delay', 'experiment_uid'});
    
    sac_table                   = [sac_table; gaze_event_sub_table];
    writetable(sac_table, save_path);

end
end
