clear
paper_dir           = '/Users/mconn24/Box Sync/papers/2020_08_01_paper_NHP_PCN_memory/';
data_table_path     = [paper_dir 'pcn_memory_meta_database.xlsx'];
data_table          = readtable(data_table_path);

image_timing_table  = [];
for c1 = 1:size(data_table,1)
    fprintf('Trial %d\n', c1)
    
    sound_file_name       = data_table.sound_data_path{c1};
    interictal_file_name  = data_table.interictal_data_path{c1};
    seizure_file_name     = data_table.seizure_data_path{c1};
    
    % Collect sound data
    if ~isempty(sound_file_name)     
        experiment_uid      = data_table.experiment_uid(c1);
        
        subject_id          = data_table.subject_id{c1};
        year                = data_table.year(c1);
        month               = data_table.month(c1);
        day                 = data_table.day(c1);
        
        sound_path          = [paper_dir sound_file_name];
        sound_data          = load(sound_path);

        file_idx_start      = regexp(sound_path, '_\d{3}_');
        file_idx_str        = sound_path(file_idx_start+1:file_idx_start+3);

        sound_channel       = sprintf('%s_%d_%02d_%02d_%s_ns1_2000__Ch5', ...
            subject_id, year, month, day, file_idx_str);

        sound_times         = sound_data.(sound_channel).times;

        if strcmp(subject_id, 'KP')
            sound_codes     = sound_data.(sound_channel).codes;
        end

        c3 = 1;
        while c3 < size(sound_times,1)-1

            training_start      = sound_times(c3);
            training_end        = sound_times(c3+1);
            training_time       = training_end - training_start;

            blank_start         = training_end;
            blank_end           = blank_start + 10;

            test_start          = blank_end;
            test_end            = test_start + 5;

            if strcmp(subject_id, 'KP')
                trial_idx       = sound_codes(c3,1);

                if trial_idx == 0
                   trial_idx    = 20; 
                elseif trial_idx > 10
                    trial_idx   = trial_idx - 6;
                end
            else
                trial_idx       = ceil(c3/2);
            end

            image_time_row          = table(trial_idx, experiment_uid, training_start, training_end, ...
                blank_start, blank_end, test_start, test_end);

            image_timing_table  = [image_timing_table; image_time_row];
            c3 = c3+2;
        end       
        
    end
        
end

%%
save_path           = [paper_dir 'sound_database.csv'];
writetable(image_timing_table,save_path);