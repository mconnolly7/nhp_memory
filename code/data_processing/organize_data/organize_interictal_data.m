% function organize_interictal_data
paper_dir               = '/Users/mconn24/Box Sync/papers/2020_08_01_paper_NHP_PCN_memory/';

data_table_path         = [paper_dir 'pcn_memory_meta_database.xlsx'];
data_table              = readtable(data_table_path);

data_table_path         = [paper_dir 'sound_database.csv'];
sound_table             = readtable(data_table_path);

sound_interictal_table  = [];

for c1 = 1:size(sound_table,1)
    fprintf('Trial %d\n', c1)
    
    experiment_uid              = sound_table.experiment_uid(c1);
    experiment_idx              = data_table.experiment_uid == experiment_uid;
    
    subject_id                  = data_table.subject_id{experiment_idx};
    year                        = data_table.year(experiment_idx);
    month                       = data_table.month(experiment_idx);
    day                         = data_table.day(experiment_idx);
    
    interictal_file_name        = data_table.interictal_data_path{experiment_idx};
    seizure_file_name           = data_table.interictal_data_path{experiment_idx};
    
    % Collect interictal spike data
    if isempty(interictal_file_name )
        continue;
    end
    interictal_paths        = strsplit(interictal_file_name, ',');

    if sound_table.trial_idx(c1) == 2 & sound_table.experiment_uid(c1) == 51
        interictal_channel
    end
    for c2 = 1:size(interictal_paths,2)
        interictal_path     = [paper_dir interictal_paths{c2}];

        switch subject_id
            case 'Casper'

                % interictal codes are on channel 33
                % all positive numbers are valid
                interictal_channel          = strrep(interictal_paths{c2}, 'data/Casper/Casper_interictal_data/','');
                interictal_channel          = strrep(interictal_channel, '_interictal','');
                interictal_channel          = strrep(interictal_channel, '_Interictal','');
                interictal_channel          = [interictal_channel '__Ch33'];

            case 'KP'

                % interictal codes are on channel 41 
                interictal_channel          = strrep(interictal_paths{c2}, 'data/KP/KP_MAT_data/','');
                interictal_channel          = strrep(interictal_channel, '(','_');
                interictal_channel          = strrep(interictal_channel, ')','_');
                interictal_channel          = [interictal_channel '_Ch41'];
                
                if sound_table.trial_idx(c1) == 1 & sound_table.experiment_uid(c1) == 52
                    interictal_channel
                end
                
            case 'Waldo'

                % interictal codes are on channel 65                   
                interictal_channel          = strrep(interictal_paths{c2}, 'data/Waldo/Waldo_interictal_data/','');
                interictal_channel          = strrep(interictal_channel, 'interictal','');
                interictal_channel          = strrep(interictal_channel, 'Interictal','');
                interictal_channel          = [interictal_channel '_Ch65'];
        end

        interictal_data         = load(interictal_path, interictal_channel);

        interictal_codes        = interictal_data.(interictal_channel).codes(:,1);
        interictal_times        = interictal_data.(interictal_channel).times;
    
        sound_row               = sound_table(c1,:);
        training_spikes_idx     = find(interictal_times > sound_row.training_start & interictal_times < sound_row.training_end);
        training_spike_codes    = {interictal_codes(training_spikes_idx)};
        training_spike_times    = {interictal_times(training_spikes_idx)};

        blank_spikes_idx        = find(interictal_times > sound_row.blank_start & interictal_times < sound_row.blank_end);
        blank_spike_codes       = {interictal_codes(blank_spikes_idx)};
        blank_spike_times       = {interictal_times(blank_spikes_idx)};

        test_spikes_idx         = find(interictal_times > sound_row.test_start & interictal_times < sound_row.test_end);
        test_spike_codes        = {interictal_codes(test_spikes_idx)};
        test_spike_times        = {interictal_times(test_spikes_idx)};

        interictal_row          = table(training_spike_codes, training_spike_times, blank_spike_codes, blank_spike_times, test_spike_codes, test_spike_times);

        sound_interictal_row    = [sound_row interictal_row];
        sound_interictal_table  = [sound_interictal_table; sound_interictal_row];
    end
end
% end
%%
save_path           = [paper_dir 'sound_interictal_database.mat'];
save(save_path, 'sound_interictal_table');