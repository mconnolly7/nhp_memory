function aoi_table = organize_AOI_data(paper_dir, data_table, save_path)

aoi_table           = [];

for c1 = 1:size(data_table,1)
    
    aoi_file_name       = data_table.AOI_data_path{c1};
    aoi_path            = [paper_dir aoi_file_name];
    
    if isempty(aoi_file_name)
        continue;
    end
    
    % Load AOI data
    raw_aoi_table       = readtable(aoi_path);
    
    % Reshape to create matrix
    %   column is trial
    %   rows are image/phase 
    aoi_mat             = reshape(raw_aoi_table{1,2:end}, 5, [])';
    image_names        	= raw_aoi_table.Properties.VariableNames(2:5:end);
    
    % Resort the matrix based on order of presentation
    clear image_idx
    for c2 = 1:size(image_names,2)
        [start_idx, end_idx]    = regexp(image_names{c2}, '\d+');
        image_idx(c2)           = str2double(image_names{c2}(start_idx:end_idx));     
    end
    
    aoi_subtable        = array2table(aoi_mat, 'VariableNames', ...
        {'encode_A','retrieve_A','retrieve_B','retrieve_A_flip','retrieve_C_flip'});
    
    switch data_table.stimulation_pattern{c1}
        case 'sham'
            pattern_id = 0;
        case 'ring_bipolar'
            pattern_id = 1;
        case 'wide_ring_bipolar'
            pattern_id = 2;
        case 'dual_ring_bipolar'
            pattern_id = 3;
        case 'full_admes'
            pattern_id = 4;
    end
    
    experiment_uid     	= data_table.experiment_uid(c1);
    uid_subtable        = table(experiment_uid * ones(size(image_idx))', pattern_id * ones(size(image_idx))', 'VariableNames', {'experiment_uid', 'stimulation_pattern'});
    
    trial_subtable      = table(image_idx', 'VariableNames', {'trial_idx'});
    aoi_subtable        = [aoi_subtable uid_subtable trial_subtable];
    aoi_table           = [aoi_table; aoi_subtable];
end

if nargin == 3
    writetable(aoi_table,save_path);
end